# Electroshop

An online shop with django 4


## Description
This project is an online store written by Python and Django.

## Badges
<img src="https://img.shields.io/badge/Heroku-430098?style=for-the-badge&logo=heroku&logoColor=white" />
<img src="http://ForTheBadge.com/images/badges/made-with-python.svg" />
<img src="https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=django&logoColor=white" />
<img src="	https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white" />
<img src="https://img.shields.io/badge/VSCode-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white" />


## Usage
```bash
  ## Windows
  
  python -m venv venv
  venv\Scripts\activate
  pip install -r requirements.txt
  python manage.py runserver
```
```bash
## Linux

  python3 -m venv venv
  source ./venv/Scripts/activate
  pip install -r requirements.txt
  python3 manage.py runserver
```

## License
[MIT](https://github.com/amir0902-official/Electroshop/blob/master/LICENSE)

## Project status
Developing
